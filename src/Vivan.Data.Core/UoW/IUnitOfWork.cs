﻿using System;
using System.Threading.Tasks;

namespace Vivan.Data.Core.UoW
{
    public interface IUnitOfWork : IDisposable
    {
        Task<bool> CommitAsync();
    }
}
