﻿using System;
using Vivan.Data.Core.Factories;

namespace Vivan.Data.Core.Repositories
{
    public abstract class BaseRepository : IDisposable
    {
        protected readonly IDbConnectionFactory DbConnectionFactory;

        protected BaseRepository(IDbConnectionFactory dbConnectionFactory)
        {
            DbConnectionFactory = dbConnectionFactory;
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
