﻿using MySql.Data.MySqlClient;
using System.Data;

namespace Vivan.Data.Core.Factories
{
    public class MySqlConnectionFactory : IDbConnectionFactory
    {
        private readonly string _connectionString;

        public MySqlConnectionFactory(string connectionString)
        {
            _connectionString = connectionString;
        }

        public IDbConnection Create()
        {
            var connection = new MySqlConnection(_connectionString);

            var command = new MySqlCommand("SET CHARACTER SET utf8", connection);
            connection.Open();
            command.ExecuteNonQuery();
            command.CommandText = "SET NAMES utf8";
            command.ExecuteNonQuery();

            return connection;
        }
    }
}
