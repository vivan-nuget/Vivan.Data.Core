﻿using Oracle.ManagedDataAccess.Client;
using System.Data;

namespace Vivan.Data.Core.Factories
{
    public class OracleConnectionFactory : IDbConnectionFactory
    {
        private readonly string _connectionString;

        public OracleConnectionFactory(string connectionString)
        {
            _connectionString = connectionString;
        }

        public IDbConnection Create()
        {
            var connection = new OracleConnection(_connectionString);
            connection.Open();

            OracleGlobalization info = connection.GetSessionInfo();
            info.TimeZone = "America/Sao_Paulo";
            connection.SetSessionInfo(info);

            return connection;
        }
    }
}
