﻿using System.Data;

namespace Vivan.Data.Core.Factories
{
    public interface IDbConnectionFactory
    {
        IDbConnection Create();
    }
}
