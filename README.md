# Vivan.Data.Core

Biblioteca que possui um conjunto de classes utilizadas para auxiliar no desenvolvimento da camada "Infra.Data" e Repositórios que acessam algum banco de dados.

## Instalação

Instalar o package **'Vivan.Data.Core'** através do Nuget Package Manager.

## Dependências

## Recursos
* IDbConnectionFactory
* OracleConnectionFactory
* BaseRepository

## Utilizando os recursos IDbConnectionFactory e OracleConnectionFactory

As classes IDbConnectionFactory e OracleConnectionFactory servem como recurso de conexão para a classe **BaseRepository**.

A interface IDbConnectionFactory deve ser adicionada no container do .net core na classe startup da seguinte forma:


```c#

services.AddScoped<IDbConnectionFactory>(factory => new OracleConnectionFactory(_configuration["ConnectionStrings:Oracle"]));

```

Note que no momento em que a classe IDbConnectionFactory é adicionada, passamos como parâmetro uma instância da classe **OracleConnectionFactory** que recebe em seu construtor a connection string da base Oracle.


## Utilizando a classe BaseRepository

É a classe base para construção de repositórios de banco de dados.

Para Instânciá-la, basta herdá-la e informar uma interface IDbConnectionFactory como parâmetro em seu construtor:

```c#

public class CatalogoRepository : BaseRepository, ICatalogRepository
{
    public CatalogoRepository(IDbConnectionFactory dbConnectionFactory) : base(dbConnectionFactory)
    { }
}

```

Para executar algum comando SQL em um repositório que herda a classe **BaseRepository**, basta abrir uma conexão com o banco de dados:



```c#

public async Task<Catalogo> ObterPorIAsync(int id)
{
    using (var connection = DbConnectionFactory.Create())
    {
        var catalog = await connection.QuerySingleAsync<Catalogo>(
                            "SELECT * FROM Catalogo WHERE Id = :Id",
                            new { Id = id });
                            
        return catalog;
    }
}
        
```

